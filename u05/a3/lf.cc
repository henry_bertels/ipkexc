#include <iostream>
#include <vector>
#include <map>


std::map<char,int> get_frequencies()
{
	std::map<char,int> f;
	
	int ges = 0;
	
	while (true)
	{
		unsigned char c;
		std::cin >> c;
		
		if (not std::cin)
		break;
		
		if (not std::isalpha(c))
		continue;
		++ges;
		c = std::toupper(c);
		
		f[c] += 1;
	}
	f['&'] = ges;
	return f;
	
}

void print_frequencies(const std::map<char,int>& frequencies, int ges)
{
	std::cout << "Anzahlen:" << std::endl;
	for (auto& entry : frequencies)
	{
		std::cout << entry.first << ": " << entry.second << std::endl;
	}
	std::cout << "Anteile:" << std::endl;
	for (auto& entry : frequencies)
	{
		double pb = (static_cast<double>(entry.second) / ges);
		std::cout << entry.first << ": " << pb << std::endl;
	}
}

int main(int argc,char** argv)
{
	
	std::map<char,int> freq;
	freq = 	get_frequencies();
	
	int ges = freq['&'];
	freq.erase('&');
	
	print_frequencies(freq, ges);
	
	std::cout << "Ges: " << ges << std::endl;
	
	return 0;
}
