#include <iostream>
#include "io.hh"
//#include "io.cc"
#include "statistics.hh"
//#include "statistics.cc"
#include <vector>
#include <algorithm>

int inp_seed()
{
	bool yesno;
	std::cout << "input seed (0)?" << std::endl << "random seed(1)?" << std::endl;
	std::cin >> yesno;
	if (yesno)
	{
		return random_seed();
	}
	else 
	{
		int output;
		std::cout << "seed: " << std::flush;
		std::cin >> output;
		return output;
	}
}

int inp_N()
{
	int output;
	std::cout << "amt of nrs: " << std::flush;
	std::cin >> output;
	return output;
}

double inp_min()
{
	double output;
	std::cout << "min: " << std::flush;
	std::cin >> output;
	return output;
}

double inp_max()
{
	double output;
	std::cout << "max: " << std::flush;
	std::cin >> output;
	return output;
}

int inp_k()
{
	int output;
	std::cout << "which stat. moment?" << std::endl;
	std::cin >> output;
	return output;
}



int main(int argc, char** argv)
{
	std::vector<double> vorg = uniform_distribution(inp_seed(), inp_N(), inp_min(), inp_max());
	//pv(vorg);
	int mk = inp_k();
	std::cout << "mean:" << mean(vorg) << std::endl;
	std::cout << "median: " << median(vorg) << std::endl;
	std::cout << mk << "th moment: " << moment(vorg, mk) << std::endl;
	std::cout << "standard deviation: " << standard_deviation(vorg) << std::endl;
	double testleft = std::pow(standard_deviation(vorg), 2);
	double testright = moment(vorg, 2)-std::pow(mean(vorg),2);
	std:: cout << "Test:" << std::endl << "left: " << testleft << std::endl 
	<< "right: " << testright << std::endl;
	return 0;
}
