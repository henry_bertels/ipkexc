#include "statistics.hh"
#include <iostream>
#include <algorithm>
#include <vector>


void pv (std::vector<double>& o)
{
	int i=0;
	for (double entry : o)
	{
		std::cout << entry << " ; " << std::flush;
		
		if (i<4)
		{++i;}
		else
		{std::cout << std::endl; i=0;}
	}
	std::cout << std::endl;
	
}


double mean(const std::vector<double>& v)
{
	//rauskommentiert. HW ist nur da, um cmake zu testen
	//std::cout << "Hello World" << std::endl;
	double output = 0;
	if (v.size()==0)
	{
		std::cout << "no mean, vector is empty" << std::endl;
		return 0;
	}
	for (double entry : v )
	{
		output += entry;
	}
	output = output/v.size();
	return output;
}



double median(const std::vector<double>& v)
{
	int N = v.size();
	if (N==0)
	{
		std::cout << "no median, vector is empty" << std::endl;
		return 0;
	}
	std::vector<double> vs = v;
	std::sort(vs.begin(), vs.end());
	if (N%2==1)
	{
		return vs[((N-1)/2)];
	}
	else if (N%2==0)
	{
		return ((vs[(N/2)-1]+vs[N/2])/2);
	}
	return 0;
}


double moment(const std::vector<double>& v, int k)
{
	if (v.size()==0)
	{
		std::cout << "no moment, vector is empty" << std::endl;
		return 0;
	}
	std::vector<double> vk;
	for (double entry : v )
	{
		vk.push_back(std::pow(entry, k));
	}
	return mean(vk);
}


double standard_deviation(const std::vector<double>& v)
{
	if (v.size()==0)
	{
		std::cout << "no standard deviation, vector is empty" << std::endl;
		return 0;
	}
	double mu = mean(v);
	std::vector<double> vm;
	for (double entry : v)
	{
		vm.push_back(entry-mu);
	}
	return std::pow(moment(vm, 2), 0.5);
}




















