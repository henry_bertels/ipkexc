
namespace root
{
	
	double pw(double ak, int n)
	{
		
		double result = 1;

		if (n == 0)
		{
			return 1;
		}
		while ( n > 0)
		{
			result *= ak;
			n--;
		}
		return result;
	}
	
	
	double iterative(double q, int n, int steps)
	{
		if (n < 1)
		{
			std::cout << "" << std::endl;
			std::cout << "ungueltiges n" << std::endl;
			return 0;
		}
		
		double ak=1;
		double bruch = n; //damit die 1/n nicht abrundet
		
		if (steps == 0)
		{
			return ak;
		}
		while(steps > 0)
		{
			ak=ak + ( 1/bruch * ( ( q / pw(ak, n-1) ) - ak ) );
			
			steps--;
		}
		
		return ak;
	}
	
	
	void test_root(double q, int n, int steps)
	{
		double ah = iterative(q, n, steps);
		double nein = q - pw(ah, n);
		/*
		std::cout << "q=" << q << std::endl;
		std::cout << "n=" << n << std::endl;
		std::cout << "steps=" << steps << std::endl;
		*/
		std::cout << "Fehler=" << nein << std::endl;	
	}
}
