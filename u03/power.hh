
namespace power
{
	long iterative(int q, int n)
	{

		long result = 1;
		
		if (n < 0)
		{
			std::cout << "n muss element von N_0 sein!" << std::endl;
			return 0;
		}
		if (n == 0)
		{
			return 1;
		}
		while ( n > 0)
		{
			result *= q;
			
			n--;
			
		}
		return result;
	}
		
	long betterit(int q, int n)
	{
		long result = 1;
		
		if ( n % 2 == 0)
		{
			long kurz = betterit(q, n/2);
			return kurz*kurz;
		}
		else
		{
			return iterative(q, n);
		}
	
		return result;
	}
	
	
	long recursive(int q, int n)
	{
		
		
		if (n < 0)
		{
			std::cout << "n muss element von N_0 sein!" << std::endl;
			return 0;
		}
		if (n == 0)
		{
			return 1;
		}
		if (n >0)
		{
			return recursive(q, n-1)*q;
		}
		
		return 0;
	}
}
