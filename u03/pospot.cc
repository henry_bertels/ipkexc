#include <cmath>
#include <iostream>
#include <string>
#include "power.hh"





int main(int argc , char** argv)
{
	int n;
	int q;
	
	std::cout << "n=" << std::flush;
	std::cin >> n;
	std::cout << "q=" << std::flush;
	std::cin >> q;
	
	std::cout << "iterative=" << power::iterative(q, n) << std::endl;	
	std::cout << "recursive=" << power::recursive(q, n) << std::endl;
	std::cout << "betterit =" << power::betterit(q, n) << std::endl;
	
	return 0;
}
