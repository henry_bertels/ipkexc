#include <cmath>
#include <iostream>
#include <string>
#include "root.hh"





int main(int argc , char** argv)
{
	double q;
	int n;
	int steps;
	
	std::cout << "q=" << std::flush;
	std::cin >> q;
	std::cout << "n=" << std::flush;
	std::cin >> n;
	std::cout << "steps=" << std::flush;
	std::cin >> steps;
	
	std::string f = "th";
	
	if (n == 1)
	{
		f = "st";
	}
	else if (n == 2)
	{
		f = "nd";
	}
	else if (n == 3)
	{
		f = "rd";
	}
	
	std::cout << n << f <<" root=" << root::iterative(q, n, steps) <<  std::endl;	
	
	root::test_root(q, n, steps);
	
	return 0;
	

	
}
