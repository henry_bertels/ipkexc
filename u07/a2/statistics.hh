#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

//ct = container-type
template<typename ct>
void pv (ct& o)
{
	int i=0;
	for (auto& entry : o)
	{
		std::cout << entry << " ; " << std::flush;
		
		if (i<4)
		{++i;}
		else
		{std::cout << std::endl; i=0;}
	}
	std::cout << std::endl;
	
}



//ct = container-type
template<typename ct>
double mean(const ct& v)
{
	typename ct::value_type output = 0;
	if (v.size()==0) {
	return 0;
	}
	for (auto& entry : v ) {
	output += entry;
	}
	return static_cast<double>(output)/v.size();
}


//ct = container-type
template<typename ct>
double median(const ct& v)
{
	int N = v.size();
	if (N==0) {
	return 0;
	}
	
	auto vs = v;
	std::sort(vs.begin(), vs.end());
	if (N%2==1)
	{
		return static_cast<double>(vs[((N-1)/2)]);
	}
	else if (N%2==0)
	{
		return ((static_cast<double>(vs[(N/2)-1])
		+static_cast<double>(vs[N/2]))/2);
	}
	return 0;
}


//ct = container-type
template<typename ct>
double moment(const ct& v, int k)
{
	if (v.size()==0) {
	return 0;
	}
	
	std::vector<typename ct::value_type> vk;
	for (auto& entry : v )
	{
		vk.push_back(std::pow(entry, k));
	}
	//pv(vk);
	return mean(vk);
}



//ct = container-type
template<typename ct>
double standard_deviation(const ct& v)
{
	if (v.size()==0) {
	return 0;
	}
	double mu = mean(v);
	std::vector<double> vm;
	for (auto entry : v)
	{
		vm.push_back(static_cast<double>(entry)-mu);
	}
	//pv(vm);
	return std::sqrt(moment(vm, 2));
}


//ct = container-type
template<typename ct>
double robust_median(const ct& v)
{
	int N = v.size();
	if (N==0) {
	return 0;
	}
	
	
	std::vector<typename ct::value_type> vs = {};
	for (auto& e : v)
	{ vs.push_back(e);}
	std::sort(vs.begin(), vs.end());
	
	
	if (N%2==1)
	{
		return static_cast<double>(vs[((N-1)/2)]);
	}
	else if (N%2==0)
	{
		return ((static_cast<double>(vs[(N/2)-1])
		+static_cast<double>(vs[N/2]))/2);
	}
	return 0;
}



#endif //STATISTICS_HH

