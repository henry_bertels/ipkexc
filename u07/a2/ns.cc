#include "statistics.hh"
//#include "statistics.cc"
#include <vector>
#include <iostream>


int main(int argc, char** argv)
{
	std::vector<int> a = {1,6};
	
	pv(a);
	
	int k = 3;
	
	std::cout << "mean: " << mean(a) << std::endl;
	std::cout << "median: " << median(a) << std::endl;
	std::cout << "3rd moment: " << moment(a,k) << std::endl;
	std::cout << "std dev: " << standard_deviation(a) << std::endl;
	
	return 0;	
}
