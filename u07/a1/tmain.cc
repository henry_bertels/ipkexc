#include <iostream>
#include <vector>
#include <array>

#include "point.hh"



int main (int argc , char** argv)
{
	Point<int,3> p1;
	p1.x(1) = 1;
	p1.x(2) = 2;
	p1.x(3) = 3;
	
	std::cout << p1.x(2) << std::endl;
	
	Point<int, 2> p2;
	p2.x(1) = 2;
	p2.x(2) = 4;
	
	std::cout << p2.norm() << std::endl;
	
	return 0;
}
