#include <vector>
#include <iostream>
#include <array>

#include "point.hh"

int main (int argc, char** argv)
{
	int fail = 0;
	
	Point<double,1> p1;
	p1.x(1) = 2.5;
	if (p1[1] != 2.5)
		{fail = 1;}
	if (p1.norm() - 2.5 > 0.01)
		{
			std::cout << p1.norm() << std::endl;
			fail = 1;}
	
	
	Point<int,3> p2;
	p2[1]=3;
	p2[2]=0;
	p2[3]=4;
	if (p2.norm() != 5)
		{fail = 1;}
	
	
	return fail;
}
