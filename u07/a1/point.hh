#ifndef POINT_HH
#define POINT_HH

#include <vector>
#include <array>
#include <cmath>

template<typename Coord, int dim>
class Point {
	
	private:
	std::array<Coord,dim> _coords;
	
	using Coordinate = Coord;
	
	public:
	static const int dimension = dim;
	
	Point()
	{
		for ( auto& e : _coords)
			e = 0;
	}
	
	Point(std::array<Coord,dim> coords)
	: _coords(coords)
	{}
	
	Coord& x(int i) {
	return _coords.at(i-1);
	}
	
	auto norm() const {
		auto sum = 0;
		for (const auto& e : _coords) {
			sum+= e*e;
		}
		return std::sqrt(sum);
	}
	
	Coord& operator[](int i) {
		return x(i);
	}
	
	/*const Coord& operator[](int i) {
		return x(i);
	}*/
	
	
	
};

#endif //POINT_HH
