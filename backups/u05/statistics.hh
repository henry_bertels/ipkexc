#ifndef STATISTICS_HH
#define STATISTICS_HH

//prints vector to std::cout
void pv(std::vector<double>& o);

double mean(const std::vector<double>& v);

double median(const std::vector<double>& v);

double moment(const std::vector<double>& v, int k);

double standard_deviation(const std::vector<double>& v);

#endif //STATISTICS_HH
