#include <iostream>
#include "io.hh"
#include "io.cc"
#include "statistics.hh"
#include "statistics.cc"

int inp_k()
{
	int output;
	std::cout << "which stat. moment?" << std::endl;
	std::cin >> output;
	return output;
}


int main(int argc, char** argv)
{
	std::vector<double> vorg = read_vector();
	//pv(vorg);
	int mk = inp_k();
	std::cout << "mean:" << mean(vorg) << std::endl;
	std::cout << "median: " << median(vorg) << std::endl;
	std::cout << mk << "th moment: " << moment(vorg, mk) << std::endl;
	std::cout << "standard deviation: " << standard_deviation(vorg) << std::endl;
	double testleft = std::pow(standard_deviation(vorg), 2);
	double testright = moment(vorg, 2)-std::pow(mean(vorg),2);
	std:: cout << "Test:" << std::endl << "left: " << testleft << std::endl 
	<< "right: " << testright << std::endl;
	return 0;
}
