#include <cmath>
#include <iostream>
#include <string>

int main(int argc, char** argv) 
{
	double a;
	jump: //rückkehrpunkt für erneute Eingabe von a
		std::cout << "a=" << std::flush; // Zeile 9f fragen a ab
		std::cin >> a;
		
		if (a==0) //dieses if dient um a=0 zu verhindern, da sonst durch 0 geteilt wird
		{
			std::cout << "a darf nicht 0 sein!" << std::endl;
			goto jump;
		}
	
	double b; // Zeile 18-24 fragen b und c ab
	std::cout << "b=" << std::flush;
	std::cin >> b;
	
	double c;
	std::cout << "c=" << std::flush;
	std::cin >> c;
	
	
	double Inhalt = (b*b)-(4*a*c); //das hier soll der Inhalt der wurzel aus b²-4ac sein
	
	if (Inhalt<0) //hier wird verhindert, dass die Wurzel undefiniert ist
	{
		std::cout << "Wurzel undefiniert" <<std::endl;
		return 0;
	}
	
	double x_1 = (-b + std::sqrt(Inhalt))/(2*a); //hier der Rest der Formel mit "+Wurzel"
	double x_2 = (-b - std::sqrt(Inhalt))/(2*a); //hier der Rest mit "-Wurzel"
	
	
	/*printen von einzelnen sachen,
	 *die als Kommentar markierten Teile habe
	 *ich nur zum Prüfen der Eingabenfunktion benutzt
	*/
	std::cout << "" << std::endl;
	//std::cout << "Values" << std::endl;
	
	//std::cout << "a=" << a << std::endl;
	//std::cout << "b=" << b << std::endl;
	//std::cout << "c=" << c << std::endl;
	//std::cout << "Inhalt: " << Inhalt << std::endl;
	std::cout << "x_1=" << x_1 << std::endl;
	std::cout << "x_2=" << x_2 << std::endl;
	
	return 0;


}


