#include <cmath>
#include <iostream>
#include <string>

/*Es gab von Anfang an keine Rechenzeitprobleme bei großen N
 *(auch ohne -03, was aber auch nicht funktioniert hat (evtl nochmal erklären))
 *
 * f war zu groß für "long" bei N>93
*/
int fibonacci()
{
	// n definieren und abfragen
	int n;
	std::cout << "N=" << std::flush;
	std::cin >> n;
	//n<=0 verhindern
	if (n <= 0)
	{
		std::cout << "n muss mindestens 1 sein" << std::endl;
		return 0;
	}
	
	if (n>93)
	{
		std::cout << "Fuer f(N)>93 ist das Ergebnis zu gross fuer eine long variable" << std:: endl;
		return 0;
	}
		
	//festlegen von f_1 und f_2 als long, da sie spaeter durch
	//groessere Zahlen ersetzt werden
	long f_1 = 0;
	
	long f_2 = 1;
	
	//Manuelle Outputs für n=1;2
	if (n == 1)
	{
		std:: cout << f_1 << std:: endl;
		return 0;
	}
	
	if (n == 2)
	{
		std:: cout << f_1 << std:: endl;
		std:: cout << f_2 << std:: endl;
		return 0;
	}
	
	// Printen von ersten beiden ausgaben, damit auch automatisch
	// erstellte Serien am Anfang vollständig sind
	if (n > 2)
	{
		std:: cout << "1: " <<f_1 << std:: endl;
		std:: cout << "2: " <<f_2 << std:: endl;
	}
	
	// counter für die x-vielte Zahl der Serie
	// (fängt bei 3 an, da 1 u 2 manuell vorgesetzt werden
	int counter = 3;
	
	/* f ist das "f(n)" aus der Formel in der Aufgabenstellung
	 * f_1 ist "f(n-2)" und f_2 ist "f(n-1)"
	 * dann wird der Index der Zahl und die Zahl ausgegeben
	 * und die "f(n-1 bzw n-2)" durch die neuen Werte ersetzt.
	 * n wird mit jedem Schritt verkleinert, sodass die "while"-
	 * Schleife n-2 mal läuft. Die zwei anderen Schritte wurden
	 * ja schon im Vorhinein ausgeführt. Ausserdem wird der Index
	 * um 1 erhöht
	*/
	while (n>2)
	{
		long f = f_1 + f_2; 
		std:: cout << counter << ": " << f << std:: endl;
		f_1 = f_2;
		f_2 = f;
		n = n-1;
		counter = counter+1;
	}	
	
	
	
	return 0;
}

/* Hier hab ich einfach die fibonacci Funktion
 * durch das int main Zeug ausführen lassen.
 * Wieso genau sollen wir das eigentlich überall nutzen?
*/

int main(int argc, char** argv) 
{
	fibonacci();
}
