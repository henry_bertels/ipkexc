#include <cmath>
#include <iostream>
#include <string>
// 0 kann nur durch Null erreicht werden, da die Operation /2
// nur für 0/2 0 ist und die Operation für Ungerades n
// den Betrag von N immer erhöht.
// alle Serien von n>0 enden in ... 4,2,1 (von Wikipedia)

void collatz()

{
	//hier wird n definiert, abgefragt und als erste Zahl ausgegeben
	int n; 
	std::cout << "n=" << std::flush;
	std::cin >> n;
	std::cout << "Number: " << n <<std::endl; 
	
	/*solange n nicht einer der Vorgegebenen Werte ist,
	 * werden die Umformungen gemacht
	 * if ist der Gerade-Fall, else der Ungerade-Fall
	 * nach jeder Umformung wird die neue Nummer ausgegeben
	*/
	while (n !=1 and n !=0 and n !=-1 and n != -5 and n !=-17)
	{
		if (n % 2 == 0)
		{
			n = n/2;
		}
		else
		{
			n= (n*3) + 1;
		}
		std::cout << "Number: " << n <<std::endl;
	}

}

/* Hier hab ich einfach die Funktion
 * durch das int main Zeug ausführen lassen.
 * Wieso genau sollen wir das eigentlich überall nutzen?
*/
int main(int argc, char** argv)
{
	collatz();
}
