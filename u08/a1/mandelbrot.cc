#include <iostream>
#include <vector>
#include <cmath>
#include "point.hh"
#include "canvas.hh"
#include "iteration.hh"


void mandelbrot 	(Canvas& canvas, double threshold, int maxIt, std::string filename)
	{
		Point z_0;
		
		int breit = canvas.hp();
		int hoch = canvas.vp();
		int i = 0;
		int j = 0;
		//std::cout << hoch << breit << std::endl;
		while (i < breit)
		{
			j=0;
			while (j < hoch)
			{
				IterationResult result = iterate(z_0, 
				canvas.coord(i,j), threshold, maxIt);
				
				int operations = result.read_op();
				//std::cout << operations << std::endl;
				if (operations == maxIt)
				{	canvas.setBrightness(i,j,0);	}
				else
				{
					double log = std::log(operations)*100;
					canvas.setBrightness(i,j,static_cast<int>(log));
				}
				//if(i>2998) {std::cout << "j=" << j << std::endl;}
				++j;
			}
			//std::cout << "x<=" << i << " done ; " << std::flush;
			//if (i%5==4) {std::cout << std::endl;}
			if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
			++i;
		}
		std::cout << "writing to file..." << std::endl;
		canvas.write(filename);
	}

int main(int argc, char** argv)
{
	std::string filename = "mandelbrot";
	Canvas canvas(Point(-1,0),4,3,4000,3000);

	mandelbrot(canvas, 1, 1000, filename);
	
	return 0;
}
