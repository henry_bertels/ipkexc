#ifndef ITERATION_HH
#define ITERATION_HH

#include <iostream>
#include <vector>
#include <cmath>
#include "point.hh"
#include "canvas.hh"



class IterationResult{
	
	private:
	Point _lastpoint;
	int _op;
	
	public:
	IterationResult()
	: _lastpoint(Point(0,0)), _op(0)
	{}
	
	IterationResult(Point lp_in, int op_in)
	: _lastpoint(lp_in), _op(op_in)
	{}
	
	void set_lp(Point& inp)
	{ _lastpoint = inp; }
	
	void set_op(int& inp)
	{ _op = inp;}
	
	Point read_lp() const
	{ return _lastpoint;}
	
	int read_op() const
	{ return _op;}
	
};

bool checkfct(Point z, double threshold,int i,int maxIt)
{
	if ((std::sqrt(z.x()*z.x() + z.y()*z.y()) < threshold) and i < maxIt)
	{	return 1;}
	else
	{ 	return 0;}
}

IterationResult iterate (Point z, Point c, double threshold, int maxIt)
{
	int i = 0;
	double x, y;
	
	while (checkfct(z,threshold,i,maxIt))
		{
			x = z.x();
			y = z.y();
			z.ex( x*x - y*y + c.x());
			z.ey( 2*x*y + c.y());
			++i;
		}
	
	return IterationResult(z,i);
}


#endif // ITERATION_HH
