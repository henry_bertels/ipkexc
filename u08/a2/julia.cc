#include <iostream>
#include <vector>
#include <cmath>
#include "point.hh"
#include "canvas.hh"
#include "iteration.hh"

void julia (Point c, Canvas& canvas, double threshold, int maxIt, std::string filename)
{
	int breit = canvas.hp();
	int hoch = canvas.vp();
	int i = 0;
	int j = 0;
	//Point c = canvas.cent();
	while (i<breit)
	{
		j = 0;
		while (j<hoch)
		{
			IterationResult result = iterate(canvas.coord(i,j), c,
												threshold , maxIt);
			int operations = result.read_op();
			//std::cout << operations << std::endl;
			if (operations == maxIt)
			{	canvas.setBrightness(i,j,0);	}
			else
			{
				if (operations<1) {operations = 1; }
				double log = std::log(operations)*100;
				canvas.setBrightness(i,j,static_cast<int>(log));
			}
			//if(i>2998) {std::cout << "j=" << j << std::endl;}
			
			++j;
		}
		if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
		++i;
	}
	std::cout << "writing to file..." << std::endl;
	canvas.write(filename);
}




int main(int argc, char** argv)
{
	Point c(-0.8, 0.156);
	std::string name = "julia_img";
	Canvas canvas(c,4,3,4000,3000);
	julia(c, canvas, 1, 1000, name);
	
	Point alternative(0.285, 0.01);
	std::string altname = "julia_img2";
	julia(alternative, canvas, 1, 1000, altname);
	
	return 0;
}
