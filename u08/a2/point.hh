#ifndef POINT_HH
#define POINT_HH

class Point{
	double _x, _y;
	
	public:
	
	// default Constructor
	Point();
	
	
	// coord Constructor
	Point(double i_x , double i_y);
	
	double x() const;
	double y() const;
	
	void ex(double i);
	void ey(double i);
	
	
};




#endif //POINT_HH
