#include <iostream>
#include <vector>
#include <cmath>
#include "point.hh"
#include "canvas.hh"
#include "iteration.hh"


void mandelbrot 	(Canvas& canvas, double threshold, 
				int maxIt, std::string filename, bool smooth = false)
	{
		Point z_0;
		
		int breit = canvas.hp();
		int hoch = canvas.vp();
		int i = 0;
		int j = 0;
		//std::cout << hoch << breit << std::endl;
	if (smooth == false)
	{
		while (i < breit)
		{
			j=0;
			while (j < hoch)
			{
				IterationResult result = iterate(z_0, 
				canvas.coord(i,j), threshold, maxIt);
				
				int operations = result.read_op();
				//std::cout << operations << std::endl;
				if (operations == maxIt)
				{	canvas.setBrightness(i,j,0);	}
				else
				{
					double log = std::log(operations)*100;
					canvas.setBrightness(i,j,static_cast<int>(log));
				}
				//if(i>2998) {std::cout << "j=" << j << std::endl;}
				++j;
			}
			//std::cout << "x<=" << i << " done ; " << std::flush;
			//if (i%5==4) {std::cout << std::endl;}
			if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
			++i;
		}
	}
	
	else if (smooth == true)
	{
		while (i < breit)
		{
			j=0;
			while (j < hoch)
			{
				IterationResult result = iterate(z_0, 
				canvas.coord(i,j), threshold, maxIt);
				
			if (result.read_op() == maxIt or result.read_op()== 0 )
				{	canvas.setBrightness(i,j,0);	}
			else
			{	double x = result.read_lp().x();
				double y = result.read_lp().y();
				double k = result.read_op();
				
				double klammer = ( (std::log(std::sqrt(x*x + y*y)))/(std::log(threshold)));
				double k_bar = static_cast<double>(k)- std::log2(klammer);
				int log = static_cast<int>(k_bar*100);
				
				if (log < 0) { canvas.setBrightness(i,j,0); }
				else 		 { canvas.setBrightness(i,j,log); }
			}
				++j;
			}
			if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
			if (i > 2000 and i%10==0) {std::cout << "x<=" << i << " done" << std::endl; }
			++i;
		}
	}
		std::cout << "smooth: " << smooth << std::endl;
		std::cout << "writing to file..." << std::endl;
		canvas.write(filename);
	}

int main(int argc, char** argv)
{
	std::string filename = "mandelbrot_img";
	Canvas canvas(Point(-1,0),4,3,4000,3000);

	mandelbrot(canvas, 10, 100, filename);
	
	/*std::string filename2  = "big_mandelbrot_smooth";
	Canvas canvas2(Point(-0.5,0), 4, 3, 8000, 6000);
	
	mandelbrot(canvas2, 100, 1000, filename2, true);*/
	
	return 0;
}
