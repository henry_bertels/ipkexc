#ifndef CANVAS_HH
#define CANVAS_HH

#include <vector>
#include "point.hh"
#include "pgm.hh"

class Canvas{
	
	private:
	const int _horpx, _verpx;
	const Point _center;
	const double _hei, _wid;
	std::vector<std::vector<int>> _grs;
	
	
	public:
	
	Canvas(const Point& center, double wid, double hei, int horpx, int verpx);
	
	int brightness(int i, int j);
	
	void setBrightness(int i, int j, int bn);
	
	Point coord(int i, int j) const;
	
	void write(const std::string& filename);
	
	int hp();
	int vp();
	
	Point cent() const;
	
};




#endif // CANVAS_HH
