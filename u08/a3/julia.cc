#include <iostream>
#include <vector>
#include <cmath>
#include "point.hh"
#include "canvas.hh"
#include "iteration.hh"

void julia (Point c, Canvas& canvas, double threshold, int maxIt, std::string filename, bool smooth = false)
{
	int breit = canvas.hp();
	int hoch = canvas.vp();
	int i = 0;
	int j = 0;
	//Point c = canvas.cent();
	
if (smooth == false)
{
	while (i<breit)
	{
		j = 0;
		while (j<hoch)
		{
			IterationResult result = iterate(canvas.coord(i,j), c,
												threshold , maxIt);
			int operations = result.read_op();
			//std::cout << operations << std::endl;
			if (operations == maxIt)
			{	canvas.setBrightness(i,j,0);	}
			else
			{
				if (operations<1) {operations = 1; }
				double log = std::log(operations)*100;
				canvas.setBrightness(i,j,static_cast<int>(log));
			}
			//if(i>2998) {std::cout << "j=" << j << std::endl;}
			
			++j;
		}
		if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
		++i;
	}
}
else if (smooth == true)
{
	while (i<breit)
	{j = 0;
		while (j<hoch)
		{
			IterationResult result = iterate(canvas.coord(i,j), c,
												threshold , maxIt);
			
			if (result.read_op() == maxIt or result.read_op()== 0 )
			{	canvas.setBrightness(i,j,0);	}
			else
			{	double x = result.read_lp().x();
				double y = result.read_lp().y();
				double k = result.read_op();
				
				double klammer = ( (std::log(std::sqrt(x*x + y*y)))/(std::log(threshold)));
				double k_bar = static_cast<double>(k)- std::log2(klammer);
				int log = static_cast<int>(k_bar*100);
				
				if (log < 0) {canvas.setBrightness(i,j,0); }
				else 		 {canvas.setBrightness(i,j,log); }
			}
			++j;
		}
		
		if (i%100==99) {std::cout << "x<=" << i << " done" << std::endl;}
		++i;
	}
}
	std::cout << "smooth: " << smooth << std::endl;
	std::cout << "writing to file..." << std::endl;
	canvas.write(filename);
}




int main(int argc, char** argv)
{
	Point c(-0.8, 0.156);
	std::string name = "julia_img";
	Canvas canvas(c,4,3,4000,3000);
	julia(c, canvas, 100, 1000, name);
	
	Point alternative(0.285, 0.01);
	std::string altname = "julia_img2";
	julia(alternative, canvas, 100, 1000, altname);
	
	/*
	std::string name_smooth = "julia_img_smooth";
	julia(c, canvas, 100, 1000, name_smooth, true);
	
	std::string altname_smooth = "julia_img2_smooth";
	julia(alternative, canvas, 100, 1000, altname_smooth, true);
	*/
	
	
	
	
	/*
	std::string big_name_smooth = "big_julia_smooth";
	Canvas canvas2(c,4,3,16000,12000);
	julia(c, canvas2, 100, 1000, big_name_smooth, true);
	*/ 
	
	
	
	return 0;
}
