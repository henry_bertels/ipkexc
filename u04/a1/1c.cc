#include <iostream>
#include <vector>

std::vector<double> reversed(const std::vector<double>& v)
{
	std::vector<double> n(v.size());
	for (int i = 0; i < v.size() ; ++i)
	{
		n[i]= v[v.size()-i-1];
	}
	return n;
	
}

void output(const std::vector<double>& o)
{
	for (double entry : reversed(o))
	{
		std::cout << entry << " ; " << std::flush;
	}
	std::cout << std::endl;
}

int main(int argc, char** argv)
{
	std::vector<double> a = {0.1 , 0.3 , 300 , -1.23, 6};
	std::vector<double> b = {2 , 7 , 1 , 2};
	std::vector<double> c = {9};
	std::vector<double> d;
	
	std::cout << "a reversed: " << std::flush;
	output (a);	
	std::cout << "b reversed: " << std::flush;
	output (b);
	std::cout << "c reversed: " << std::flush;
	output (c);
	std::cout << "d reversed: " << std::flush;
	output (d);
}
