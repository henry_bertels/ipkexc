#include <iostream>
#include <vector>




int main(int argc, char** argv)
{
	std::vector<int> v1;
	
	std::vector<int> v2(10);
	
	std::vector<int> v3 = {{ 3, 8, 7, 5, 9, 2 }};
	
	
	std::cout << "vector 1:" << std::endl;
	for (int i = 0 ; i < v1.size() ; ++i)
	{
		std::cout << v1[i] << std::endl;
	}
	//leer
	
	
	std::cout << "vector 2:" << std::endl;
	for (int i = 0 ; i < v2.size() ; ++i)
	{
		std::cout << v2[i] << std::endl;
	}
	//10 mal "0"
	
	
	std::cout << "vector 3:" << std::endl;
	for (int i = 0 ; i < v3.size() ; ++i)
	{
		std::cout << v3[i] << std::endl;
	}
	//die elemente des vektors in reihenfolge
	
}

