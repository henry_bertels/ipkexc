#include <iostream>
#include <vector>
#include <algorithm>



int main(int argc, char** argv)
{
	std::vector<double> a = {0.1 , 0.3 , 300 , -1.23, 6};
	std::vector<int> b = {2 , 7 , 1 , 2};
	std::vector<int> c = {9};
	
	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());
	std::sort(c.begin(), c.end());
	
	
	std::cout << "kleinstes und groesstes in a" << std::endl;
	std::pair<double, double> pa = {a[0], a[a.size()-1]};
	std::cout << pa.first << std::endl;
	std::cout << pa.second << std::endl;
	
	
	std::cout << "kleinstes und groesstes in b" << std::endl;
	std::pair<double, double> pb = {b[0], b[b.size()-1]};
	std::cout << pb.first << std::endl;
	std::cout << pb.second << std::endl;
	
	
	std::cout << "kleinstes und groesstes in c" << std::endl;
	std::pair<double, double> pc = {c[0], c[c.size()-1]};
	std::cout << pc.first << std::endl;
	std::cout << pc.second << std::endl;
	
}
