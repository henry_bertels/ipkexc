#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

void reversed(std::vector<double>& v)
{
	for (int i=0 ; i < ((v.size()-(v.size() % 2))/2) ; ++i)
	{
		//std::cout << i << std::endl; 
		std::swap(v[i],v[v.size()-1-i]);
	}
}

void pv(std::vector<double>& o)
{
	for (double entry : o)
	{
		std::cout << entry << " ; " << std::flush;
	}
	std::cout << std::endl;
	
}

int main(int argc, char** argv)
{
	
	std::vector<double> a = {0.1 , 0.3, 19 , 300 , -1.23, 9.87, 6};
	std::vector<double> b = {2.4 , 7.5 , 1.6 , 2.7};
	std::vector<double> c = {9};
	std::vector<double> d;
	
	reversed(a);
	reversed(b);
	reversed(c);
	reversed(d);
	
	std::cout << "a umgekehrt: " << std::flush;
	pv(a);
	std::cout << "b umgekehrt: " << std::flush;
	pv(b);
	std::cout << "c umgekehrt: " << std::flush;
	pv(c);
	std::cout << "d umgekehrt: " << std::flush;
	pv(d);
	
	
	
	
	
}
