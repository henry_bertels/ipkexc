#include <array>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include "2p.hh"



int main(int argc, char** argv)
{
	std::string fn;
	std::cout << "input file name:" << std::flush;
	std::cin >> fn;
	std::cout << std::endl;
	
	std::string fo;
	std::cout << "output file name:" << std::flush;
	std::cin >> fo;
	std::cout << std::endl;
	
	bool yn;
	std::cout << "print steps? (true/false): " << std::flush;
	std::cin >> yn;
	std::cout << std::endl;
	
	rpff(fn,fo,yn);
}
