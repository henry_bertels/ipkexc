void pv(std::vector<double>& o)
{
	for (double entry : o)
	{
		std::cout << entry << " ; " << std::flush;
	}
	std::cout << std::endl;
	
//"printvector" nur für "alles"-vektor (double Komponenten) unten nützlich
//zum ausgeben eines vektors mit arraykomponenten nutze "pva"-funktion
}


void pva (std::vector<std::array<double,2>>& j)
{
	std::cout << "Arrays des Vektor" << std::endl; 
	for (std::array<double,2> ph : j)
	{
		for (double entry : ph)
		{
			std::cout << entry << " ; " << std::flush;
			
		}
		std::cout << std::endl;
		//jedes array belegt eine zeile, nach jeder Zahl folgt ";"
	}
	std::cout << std::endl;
	std::cout << std::endl;
	//pva gibt einen vektor aus arrays von groesse 2 in der Konsole aus
}


bool sort_by_y(std::array<double,2> a, std::array<double,2> b)
{
	if(a[1]<b[1] or (a[1]==b[1] and a[0]<b[0]))
	{
		return true;
	}
	else
	{
		return false;
	}
	//in aufg geforderter sort_by_y boolean
}


void convex_hull(std::vector<std::array<double,2>>& points)
{
	
	std::sort(points.begin(), points.end(), sort_by_y) ;
	//sortiert punkte nach y-Koordinate
}


void pushdown(std::vector<std::array<double,2>>& points)
{
	for (int i = 1; i<points.size() ; ++i)
	{
		points[i][0]=points[i][0]-points[0][0];
		points[i][1]=points[i][1]-points[0][1];
	}
	//subtrahiert p0 von jedem vektor ausser p0
}


void pushup(std::vector<std::array<double,2>>& points)
{
	for (int i = 1; i<points.size() ; ++i)
	{
		points[i][0]=points[i][0]+points[0][0];
		points[i][1]=points[i][1]+points[0][1];
	}
	//addiert p0 auf jeden vektor ausser p0
}


bool sort_by_angle(std::array<double,2> a, std::array<double,2> b)
{
	if ((a[0]*b[1]-a[1]*b[0] > 0) or ((a[0]*b[1]-a[1]*b[0] == 0) and std::abs(a[0])>std::abs(b[0])))
	{
		return true;
	}
	else
	{
		return false;
	}
	//in aufg geforderter boolean für Winkelsortierfunktion (anglesort)
}


void anglesort(std::vector<std::array<double,2>>& points)
{
	std::sort(points.begin()+1, points.end(), sort_by_angle);
	//Winkelsortierfunktion, sortiert alle vektoren ausser p0
}


bool rightleft(std::array<double,2> a, std::array<double,2> b,std::array<double,2> c)
{
	if
	( (
	((b[0]-a[0])*(c[1]-a[1])) - ((c[0]-a[0])*(b[1]-a[1])) < 0 ) or (
	((b[0]-a[0])*(c[1]-a[1])) - ((c[0]-a[0])*(b[1]-a[1])) == 0	
	) )
	{
		return true;}
	
	else 
	{return false;}
	//Boolean, um zu bestimmen ob c rechts von a-->b ist
	
}


void GrahamScan(std::vector<std::array<double,2>>& pm)
{
	int i = 1;
	
	while (i<pm.size() or i==pm.size())
	{
		if (rightleft(pm[i-1],pm[i+1],pm[i]))
		{
			++i;
		}
		else
		{
			pm.erase(pm.begin()+i);
			--i;
		}
	}
	//Graham Scan nach PseudoCode auf Wikipedia
}


std::vector<std::array<double,2>> rpff(std::string filename, std::string ofile, bool yesno)
{
	std::vector<std::array<double,2>> o;
	std::ifstream dat (filename);
	
	std::vector<double> alles;
	double inp;
	while (dat >> inp)
	{
		alles.push_back(inp);
	}
	//pv(alles);
	
	
	std::array<double,2> t;
	for (int i=0; i<alles.size() ; ++i)
	{
		if (i%2==0)
		{
			t[0]=alles[i];
			
		}
		else
		{
			t[1]=alles[i];
			o.push_back(t);
			//pva(o);
		}
		
		
	}
	if (yesno) {pva(o);}
	
	convex_hull(o);
	if (yesno) {std::cout << "nach convex_hull:" << std::endl; pva(o);}
	
	pushdown(o);
	if (yesno) {std::cout << "nach pushdown:" << std::endl; pva(o);}
	
	anglesort(o);
	if (yesno) {std::cout << "nach anglesort:" << std::endl; pva(o);}
	
	pushup(o);
	if (yesno) {std::cout << "nach pushup" << std::endl; pva(o);}
	
	GrahamScan(o);
	if (yesno) {std::cout << "nach graham scan" << std::endl; pva(o);}
	
	
	//hiernach: Drucken
	std::ofstream file (ofile);
	
	for (std::array<double,2> pkt : o)
	{
		file << pkt[0] << " " << pkt[1] << std::endl;
	}
	
	file.close();
	
	return o;
}



