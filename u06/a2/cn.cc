#include <iostream>
#include <vector>
#include "point.hh"
#include "canvas.hh"
#include "pgm.hh"

int main(int argc, char** argv)
{
	int x = 5;
	int y = 5;
	int breit = 5;
	int hoch = 5;
	
	Point c(0,0);
	Canvas test(c, breit, hoch, x, y);
	
	Point a = test.coord(5,5);
	std::cout << a.x() << std::endl;
	std::cout << a.y() << std::endl;
	
	int i = 0;
	while (i<x)
	{
		int j = 0;
		while (j<y)
		{
			test.setBrightness(i,j,((i+j)%2) );
			++j;
		}
		++i;
	}
	
	
	std::cout << test.brightness(1,2) << std::endl;
	
	return 0;
}
