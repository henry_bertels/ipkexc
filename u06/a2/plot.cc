#include <iostream>
#include "canvas.hh"
#include "point.hh"
#include <cmath>
#include <string>


int main(int argc, char** argv)
{
	Point c(0,0);
	Canvas img(c,4,3,4000,3000);
	
	int i = 0;
	while (i<4000)
	{
		int j = 0;
		while (j<3000)
		{
			double si_1 = sin(1/(img.coord(i,j).x()));
			double si_2 = sin(1/(img.coord(i,j).y()));
			double fct = (100*si_1*si_2 +1);
			int f = std::max(0, static_cast<int>(fct));
			
			img.setBrightness(i,j,f);
			++j;
		}
		++i;
	}
	//std::cout << "_grs finished" << std::endl;
	
	
	std::string filename;
	std::cout << "Filename?" << std::endl;
	std::cin >> filename;
	img.write(filename);
	
	return 0;
}
