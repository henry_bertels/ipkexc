#include <vector>
#include <iostream>
#include "point.hh"
#include "canvas.hh"
#include "pgm.hh"

int main (int argc, char** argv)
{
	int x = 4000;
	int y = 3000;
	int breit = 4;
	int hoch = 3;
	Point cent(1,1);
	
	Canvas t(cent, breit, hoch, x, y);
	
	double hw = static_cast<double>(breit)/2;
	double hh = static_cast<double>(hoch)/2;
	
	bool fail = 0;
	
	Point lu = t.coord(0,0);
	if (	(lu.x() != (cent.x()-hw))
		or 	(lu.y() != (cent.y()-hh)))
	{
		std::cout << "links unten failed" << std::endl;
		fail = 1;
	}
	
	Point ru = t.coord(x,0);
	if  (	(ru.x() != (cent.x()+hw))
		or	(ru.y() != (cent.y()-hh)))
	{
		std::cout << "rechts unten failed" << std::endl;
		fail = 1;
	}
	
	Point lo = t.coord(0,y);
	if (	(lo.x() != (cent.x()-hw))
		or 	(lo.y() != (cent.y()+hh)))
	{
		std::cout << "links oben failed" << std::endl;
		fail = 1;
	}
	
	Point ro = t.coord(x,y);
	if  (	(ro.x() != (cent.x()+hw))
		or	(ro.y() != (cent.y()+hh)))
	{
		std::cout << "rechts oben failed" << std::endl;
		fail = 1;
	}
	if (fail == 0)
	{ std::cout << "no failures for corners" << std::endl; }
	return fail;
	
}
