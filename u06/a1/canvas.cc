#include "canvas.hh"
#include <iostream>
#include <vector>
#include "pgm.hh"

Canvas::Canvas(const Point& center, double wid, double hei, int horpx, int verpx)
: _center(center) , _wid(wid) , _hei(hei) , _horpx(horpx) , _verpx(verpx) 
{}

int Canvas::brightness(int i, int j)
{
	if (_grs.size() == 0)
	{return 0;}
	
	std::vector<int> a = _grs.at(i);
	return a[j];
}

void Canvas::setBrightness(int i, int j, int bn)
{
	if (_grs.size() == 0)
	{
		std::cout << "_grs was empty, now resized" << std::endl;
		_grs.resize(_horpx);
		for (auto& entry : _grs)
		{
			entry.resize(_verpx);
		}
		std::cout << _grs.size() << std::endl;
		std::cout << _grs[1].size() << std::endl;
	}
	
	if (_grs.size() != 0) 
	{
		_grs[i][j] = bn;
	//std::cout << vec[j] << std::endl;
	}
	
}

Point Canvas::coord(int i, int j) const 
{
	double deltx = _wid/_horpx;
	double delty = _hei/_verpx;
	return Point(deltx*i + _center.x() -(_wid/2), delty*j + _center.y() - (_hei/2));
}

void Canvas::write(const std::string& filename)
{
	write_pgm( _grs ,filename);
}


