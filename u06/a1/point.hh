#ifndef POINT_HH
#define POINT_HH

class Point{
	double _x, _y;
	
	public:
	
	// default Constructor
	Point();
	
	
	// coord Constructor
	Point(double i_x , double i_y);
	
	double x() const;
	double y() const;
	
	
};




#endif //POINT_HH
