#include <iostream>
#include "point.cc"
#include "point.hh"
#include <vector>
#include "polygon.hh"
#include "polygon.cc"
#include <cmath>


int main(int argc, char** argv)
{
	const double pi = M_PI;
	int n = 0;
	std::vector<Point> v;
	while (n<11)
	{
		int i = 0;
		while (i<n)
		{
			double d = i;
			double x = cos((d/n)*2*pi);
			double y = sin((d/n)*2*pi);
			//std::cout << "x: " << x << "	y: "<< y << std::endl;
			Point a(x,y);
			v.push_back(a);
			++i;
		}
		Polygon regular(v);
		double ap = n;
		double approx = (ap/2)*sin(2*pi/ap);
		std::cout << "vector size: " << v.size() << std::endl;
		std::cout << "Volume: " << regular.volume() << std::endl;
		std::cout << "approx: " << approx << std::endl;
		std::cout << std::endl;
		++n;
		v.clear();
	}
	
	
	// ueberschlagen gibt 0, da zwei gleich große Flächen mit versch Vorzeichen
	// voneinander abgezogen werden (Vorzeichen wechseln, da links/rechts-Relation
	// durch ueberschlag nicht konstant ist)
	std::vector<double> xs = {0,1,0,1};
	std::vector<double> ys = {0,0,1,1};
	Polygon ub(xs,ys);
	std::cout << "ueberschlagen: " << ub.volume() << std::endl;
	
	return 0;
}
