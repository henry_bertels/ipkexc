#include "polygon.hh"
#include "point.hh"
#include <vector>

	Polygon::Polygon(const std::vector<Point>& corners)
	: _corners(corners)
	{}
	
	Polygon::Polygon(const std::vector<double>& x,
	const std::vector<double>& y)
	{
		_corners.clear();
		
		int n = std::max(x.size() , y.size());
		int i = 0;
		while (i<n)
		{
			Point ph(x[i], y[i]);
			_corners.push_back(ph);
			
			++i;
		}
	}
	
	std::size_t Polygon::corners() const 
	{
		return _corners.size();
	}
	
	const Point& Polygon::corner(std::size_t i) const
	{
		return _corners[i];
	}
	
	
	double Polygon::volume() const
	{
		
		int n = _corners.size();
		if (n<3)
		{ return 0; }
		
		double o = 0;
		int i = 0;
		
		while (i<n-1)
		{
			o += 	((_corners[i].x() * _corners[i+1].y()) 
					- (_corners[i+1].x() * _corners[i].y()));
			++i;
		}
		o += 	((_corners[n-1].x() * _corners[0].y()) 
				- (_corners[0].x() * _corners[n-1].y()));
		
		o = o/2;
		return o;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
